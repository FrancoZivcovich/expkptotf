function [ F, m, s, Cp ] = expkptotf( A, prec, evSch, Cp_max )
% function [ F, m, s, Cp ] = EXPKPTOTF( A, prec, evSch, Cp_max )
%
% Inputs
%              A:  n x n matrix to exponentiate
% Optional
%           prec:  precision up to which exp( A ) is approximated
%          evSch:  if false, Paterson-Stockmeyer evaluation scheme is employed
%         Cp_max:  maximum matrix products allowed to evaluate T_m(2^-s A)
%
% Outputs
%              F:  approximation of the matrix exponential of A
%              m:  degree of approximation used
%              s:  log2 of the employed scaling parameter
%             Cp:  total matrix by matrix products
%
%
%  Version updated to February the 15th, 2020.
%
%  For questions, mail to Franco Zivcovich at franco.zivcovich@gmail.com
%
%  REFERENCES: On-the-fly Krylov projection of the backward error for arbitrary
%              precision Taylor approximation of the matrix exponential
%
%              Submitted in 2020.
% Copyright (c), Franco Zivcovich. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%   * Redistributions of source code must retain the above copyright
%     notice, this list of conditions and the following disclaimer.
%   * Redistributions in binary form must reproduce the above copyright
%     notice, this list of conditions and the following disclaimer in the
%     documentation and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT,
% INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
% PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
% LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
% NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%
% Inputs Handling
%
% 1st input
if diff( size( A ) )
  error( 'input matrix must be squared' );
end

% 2nd input
u = eps( ones( 1,1, 'like', A ) ) / 2;
if ( ( nargin < 2 ) || isempty( prec ) )
  prec = u;
end

% 3rd input
if ( ( nargin < 3 ) || isempty( evSch ) )
  evSch = ( prec > 2^-54 ); % false = PatSto scheme
end

% 4th input
can_touch = ( ( nargin < 4 ) || isempty( Cp_max ) || ( Cp_max < 1 ) );
if ( evSch )
  % Sastre evaluation scheme
  newEval = [ 2 4 8 15 21 24 30; 2 2 2 2 3 4 5 ]; % < - - - - new degrees can be added here
  opt_m = @( Mp, zed ) newEval( 1,Mp );
  opt_z = @( Mp ) newEval( 2,Mp );
  % set 4th input: Cp_max
  if ( can_touch || ( Cp_max > size( newEval, 2 ) ) )
    if ( prec >= 2^-24 )
      Cp_max = 5;
    else
      Cp_max = size( newEval, 2 );
    end
  end
  n_max = bea_tay_approx( opt_m( Cp_max, opt_z( Cp_max ) ), log( prec ) );
else
  % Paterson--Stockmeyer evaluation scheme
  opt_m = @( Mp, zed ) ( Mp - zed + 2 ) * zed;
  opt_z = @( Mp ) ceil( Mp / 2 ) + 1;
  % set 4th input: Cp_max
  if ( can_touch )
    if ( prec >= 2^-24 )
      Cp_max = 7;
    else
      Cp_max = 9;
    end
  end
  n_max = bea_tay_approx( opt_m( Cp_max, opt_z( Cp_max ) ), log( prec ) );
  if ( ( prec < 2^-53 ) && ( can_touch ) )
    aux = bea_tay_approx( opt_m( Cp_max + 1, opt_z( Cp_max + 1 ) ), log( prec ) );
    while ( ( aux >= 1.45 * n_max ) || ( n_max < 1 ) )
      Cp_max = Cp_max + 1;
      n_max = aux;
      aux = bea_tay_approx( opt_m( Cp_max + 1, opt_z( Cp_max + 1 ) ), log( prec ) );
    end
  end
end
Cp = min( 2, Cp_max );
z = opt_z( Cp );
m = opt_m( Cp, z );

%
% Shifting, Arnoldi iteration and Scaling
%
% Shifting
n_A = norm( A,1 );
n = length( A );
mu = full( trace( A ) ) / n;
A( 1:n+1:n^2 ) = A( 1:n+1:n^2 ) - mu;

% Arnoldi iteration
M = 8;
w = A * randn( n,1 ); n_w = norm( w ); w = w / n_w;
fastee = ( ~isinf( n_w ) && ~isnan( n_w ) && ( n_w ~= 0 ) );
if ( fastee )
  if isnumeric( A )
    skewness = ( ishermitian( A ) - ishermitian( A, 'skew' ) );
  else
    skewness = 0;
  end
  [ V, x, rly ] = my_krylov_diag( A, w, M, skewness ); M = length( x );
  e1 = V \ [ 1; zeros( M-1, 1 ) ]; % we'll need that later
else
  x = nthroot( lazy_est1 ( @Af, A, 1, M, 1, Inf ), M );
end

% Scaling
s = max( ceil( log2( norm( x, inf ) / n_max ) ), 0 );

%
% Fast Krylov BEE
%
tol = min( 1, prec * 2^-s * n_A );
if ( fastee )
  all_ref = true; i_o = false;
  x = 2^-s * x; log_x = log( x ); exp_x = exp( -x );
  it = 0; maxit = 1e2;
  while ( true )
    while ( ( ~i_o ) && ( it < maxit ) )
      it = it + 1;

      stash = exp( ( m + 1 ) * log_x - gammaln( m + 2 ) );
      aux = stash;
      j = m + 1;
      while ( norm( stash ./ aux,inf ) > 1e-5 )
        j = j + 1;
        stash = x .* stash / j;
        aux = aux + stash;
      end
      aux = V * ( log1p( - exp_x .* aux ) .* e1 );

      i_o = ( norm( aux, 2 ) / sqrt( n ) < tol );
      if ( Cp == Cp_max ) && ( ~i_o )
        all_ref = false;
        s = s + 1;
        x = x / 2; log_x = log_x - log( 2 ); exp_x = sqrt( exp_x );
        tol = min( 1, prec * max( 1, 2^-s * n_A ) );
        Cp = min( 2, Cp_max ); % let's start from scratch m
      elseif ( ~i_o )
        Cp = Cp + 1;
      end
      z = opt_z( Cp ); m = opt_m ( Cp, z );
    end
    %
    % Scaling refinement
    %
    if ( s > 0 && all_ref )
      s = s - 1;
      x = 2 * x; log_x = log_x + log( 2 ); exp_x = exp_x.^2;
    else
      break,
    end
  end
end
i_o = false;

%
% Slow classical BEE
%
l = 1;
B{ 1 } = full( 2^-s * A );
while ( l < z )
  l = l + 1;
  B{ l } = B{ ceil( l / 2 ) } * B{ floor( l / 2 ) };
end
Bz = [ nthroot( norm( B{ z-1 },1 ), z-1 ), nthroot( norm( B{ z },1 ), z ) ]; beta = max( Bz );
c_o = ( ( z > 8 ) && ~isinf( n_w ) && ~isnan( n_w ) && ( n_w ~= 0 ) );
if c_o
  for j = 1:floor( m / z )
    w = B{ z } * w;
  end
  if ( m - z * floor( m / z ) )
    w = B{ m - z * floor( m / z ) } * w;
  end
end
it = 0; maxit = 1e3;
while ( ( ~i_o ) && ( it < maxit ) )
  it = it + 1;
  i_o = bee_check_tay( B, z, m, tol, w, c_o, Bz( 2 ), beta );
  if ( Cp == Cp_max ) && ( ~i_o )
    for j = 1:z
      B{ j } = B{ j } * 2^-j;
    end
    Bz = Bz / 2; beta = beta / 2;
    if c_o
      w = 2^( -m ) * w;
    end
    s = s + 1; tol = min( 1, prec * max( 1, 2^-s * n_A ) );
    % we do not restart from a scratch m we computed B{z}
  elseif ( ~i_o )
    Cp = Cp + 1; z = opt_z( Cp ); m = opt_m ( Cp, z );
    if c_o
      w = B{ opt_z( Cp - 1 ) } * w;
    end
    while ( l < z )
      l = l + 1;
      B{ l } = B{ ceil( l / 2 ) } * B{ floor( l / 2 ) };
      Bz = [ Bz( 2 ), nthroot( norm( B{ l },1 ), l ) ];
    end
    beta = max( Bz );
  end
end

%
% Evaluation part
%

% Compute T_m( 2^-s * A )
if ( evSch )
  F = EFFEVPOL( B{ 1 }, B, m, z, n );
else
  F = MPS( B, m, z, 1, n );
end

% Shift back and Squaring
if ( real( mu ) < 0 )
  F = F * exp ( 2^-s * mu );
end
for j = 1:s
  F = F * F;
  Cp = Cp + 1;
end
if ( real( mu ) >= 0 )
  F = F * exp( mu );
end

%
% USED FUNCTIONS
%

%
function c = bea_tay_approx( m, log_tol )
% Compute the under-estimate of the theta_m relative to the Taylor approximation
% of degree m with tolerance tol.
%
tol = log_tol + gammaln( m + 2 );
a = 0; b = 100; c = 0; c0 = 1;
it = 0; maxit = max( 100, abs( log_tol ) );
while ( ( abs( c0 - c ) > ( c * 1e-2 ) ) && ( it < maxit ) )
  it = it + 1;
  c0 = c;
  c = ( a + b ) / 2;
  if  ( ( c + m * log( c ) ) < tol )
    a = c;
  else
    b = c;
  end
end
%endfunction

%
function i_o = bee_check_tay( B, z, m, tol, w, c_o, Bz, beta )
% It checks whether the Paterson--Stockmeyer triangular inequality backward
% error is smaller than the tolerance t.
%

threshold = exp( gammaln( m + 1 ) + log( tol ) );
i_o = false; p = 0;
hcoef = 0 * tol + 1; est = 0 * tol; % we force them to inherit tol data type
while ( p < 1e2 ) && ( est < threshold )

  % Form P
  P = B{ 1 } / ( hcoef * ( m + p * z + 1 ) );
  hcoef = - hcoef * ( p * z + 1 );
  for j = 2:z
    P = P + B{ j } / ( hcoef * ( m + p * z + j ) );
    hcoef = - hcoef * ( p * z + j );
    if ( ( p == 0 ) && ( j < z ) && c_o && ( norm( P * w,1 ) > threshold ) )
      i_o = false;
      return,
    end
  end

  % Overestimate blocks...
  aux = est;
  if ( p == 0 )
    % ...sharply
    est = est + lazy_est1 ( @Af, B{ z }, P, m / z, 1, threshold - aux );
  elseif ( p == 1 )
    % ...quite unsharply
    est = est + Bm * lazy_est1 ( @Af, B{ z }, P, p, 1, ( threshold - aux ) / Bm );
  else
    % ...unsharply
    est = est + Bm * Bz^( z + p ) * norm( P, 1 );
  end

  % Happy 1: new update to est is negligible
  if ( ( aux == est ) && ( -log1p( -exp( log( est ) - gammaln( m + 1 ) ) ) < tol ) )
    i_o = true;
    return
  end

  % Happy 2: hybrid "classical" overestimate of the tail
  if ( est < threshold )
    % may have to compute Bm
    if ( p == 0 )
      Bm = lazy_est1 ( @Af, B{ z }, 1, m / z, 1, Inf );
    end
    % Prepare "classical" bea overestimate
    acc = abs( hcoef );
    aux = 0;
    hist = zeros( 1,2 );
    it = 1; maxit = 1e2;
    iidx = p * z + j + 1; lBm = log( Bm ); lbeta = log( beta );
    while ( it < maxit ) && ( ( est + Bm * aux ) < threshold )
      it = it + 1;
      iidx = iidx + 1;
      hist( it ) = exp( floor( m / iidx ) * lBm + mod( iidx, m ) * lbeta ) / ( acc * ( m + iidx - 1 ) );
      if ( 2 * hist( it ) < hist( it-1 ) )
        aux = aux + 2 * hist( it-1 );
        break,
      end
      aux = aux + hist( it );
      acc = acc * ( iidx - 1 );
    end
    aux = est + Bm * aux;

    % Check "classical" bea overestimate
    if ( ( aux < threshold ) && ( -log1p( -exp( log( aux ) - gammaln( m + 1 ) ) ) < tol ) )
      i_o = true;
      return
    end

  end

  % No luck: step ahead!
  p = p + 1;

end

%
function [ V, x, rly ] = my_krylov_diag( A, q1, m, skewness )
% Carries out m ARNOLDI iterations with n-by-n matrix A and starting vector q1.
% For m < n it produces an n-by-(m+1) matrix Q with orthonormal columns and an
% Hessemberg matrix H such that
%    A * Q(:,1:m) = Q(:,1:m) * H(1:m,1:m) + hm1 * Q(:,m+1) * e_m',
% where e_m is the mth column of the m-by-m identity matrix.
%
% It then returns V, x such that
%                      H = V * diag( x ) * inv( V ),
% rly is true if H is a real matrix.
%
%
% NB: If skewness = 1 (or skewness = -1) then H is hermitian (or skew-hermitian)
%     and the iterations are the cheaper LANCZOS ones.
%
n = length( q1 ); nq1 = norm( q1 ); q = q1 / nq1;
H = zeros( m+1 ); Q = zeros( n,m ); Q( :,1 ) = q;
for j = 1 : m
  z = A * Q( :,j );
  for k = j : -1 : max( ( j - 1 ) * abs( skewness ),1 )
    H( k,j ) = Q( :,k )' * z;
    z = z - H( k,j ) * Q( :,k );
  end
  H( j+1,j ) = norm( z );
  if H( j+1,j ) == 0
    break,
  elseif skewness,
    H( j,j+1 ) = skewness * H( j+1,j );
  end
  Q( :,j+1 ) = z / H( j+1,j );
end
% Compute what you need:
[ V, x ] = eig( H( 1:j,1:j ) ); x = diag( x ); rly = isreal( H( 1:j,1:j ) );
%endfunction

%
function est = lazy_est1 (Af, B, P, pow, s, threshold, X)
% Returns the estimate of 1-norm of matrix by block 1-norm power method. If
% the estimate exceeds tol then it stops refining it and returns it.
%
if ((nargin < 7) || any (isinf (X(:))) || any (isnan (X(:))))
  n = length (B);
  v = Af ('notransp', ones(n, 1, class(B))/n , B, P, pow, s);
  est = norm (v, 1);
else  % laziness
  v = P * X;
  est = sum (abs (v));
  [~, j] = max (est, [], 2);
  v = v(:, j);
  est = est(j);
end
if (est > threshold)
  if (strcmp ('sym', class (est)))
    est = double (est);
  end
  return
end
xi = sign (v);
x = Af ('transp', xi, B, P, pow, s);
iter = 2;
out = 0;
while ((iter < 6) && (out == 0))
  [~, j] = max (abs (x));
  ej = zeros( size( x,1 ), 1, class( x ) ); ej( j ) = 1;
  v = Af ('notransp', ej, B, P, pow, s);
  est_old = est;
  est = norm (v, 1);
  if ((est > threshold) || ((est <= est_old) || (norm (sign (v) - xi, Inf) == 0)))
    if (strcmp ('sym', class (est)))
      est = double (est);
    end
    return,
  end
  xi = sign (v);
  x = Af ('transp', xi, B, P, pow, s);
  out = ( max (abs (x)) == x(j) );
  iter = iter + 1;
end
if (strcmp ('sym', class (est)))
  est = double (est);
end
%endfunction

%
function x = Af (flag, x, A, P, i, s)
% Function auxiliary to the norm-1 estimate.
% Produces:
%        (A  / s) ^ i * P  * x (flag = 'notransp')
%        (A' / s) ^ i * P' * x (flag = 'transp')
%
switch flag
  case {'notransp'}
    if (~isempty (P))
      x = P * x;
    end
    for j = 1:i
      x = A * (x / s);
    end
  case {'transp'}
    if ~isempty (P),
      x = P' * x;
    end
    for j = 1:i
      x = A' * (x / s);
    end
end
%endfunction

%
function F = MPS( B, m, z, s, n )
% Modified Horner-Paterson-Stockmeyer scheme adapted to fit
% scaled-Taylor method with z divisor of m.
%

F = full(B {z}) / (s * m);
for j = z - 1:-1:1
  F = (F + B {j}) / (s * (m - z + j));
end
F = B{z} * F;
for k = m / z - 2:-1:1
  for j = z:-1:1
    F = (F + B {j}) / (s * (k * z + j));
  end
  F = B{z} * F;
end
for j = z:-1:1
  F = (F + B {j}) / (s * j);
end
if ((issparse (F)) && (nnz (F) > 5 * n ))
  F = full (F);
end
F(1:n + 1:n ^ 2) = F(1:n + 1:n ^ 2) + 1;
%endfunction

%
function [sol, np] = EFFEVPOL( A, Ap, m, q, n )
% Computes the exponential of matrix A by means of Taylor series
% and matrix polynomial evaluation methods from [1, 2].
%
% Inputs
%      A:  the input matrix.
%     Ap:  cell array with the powers of A nedeed to compute Taylor
%          series, such that Ap{i} contains A^i, for i=2,3,...,q.
%      m:  order or approximation to be used.
%      q:  maximum matrix power used (A,A^2,...,A^q)
%      n:  size of the square matrix A
%
% Outputs
%    sol:  the exponential of matrix A.
%     np:  matrix products carried out by the function.
%
%  Revised version 2018/01/10
%  REFERENCES: J. Sastre, Efficient evaluation of matrix polynomials,
%              Linear Algebra App., 539 (2018), 229-250.
%              https://doi.org/10.1016/j.laa.2017.11.010
switch m
  case 1 %m=1 q=1
    sol=A+eye(n);
    np = 0;
  case 2 %m=2 q=2
    sol=Ap{2}/2+A+eye(n);
    np = 0;
  case 3 %m=3 q=3 %Nilpotent matrices
    sol=Ap{3}/6+Ap{2}/2+A+eye(n);
    np = 0;
  case 4 %m=4
    if q==2
      sol=((Ap{2}/4+A)/3+eye(n))*Ap{2}/2+A+eye(n);
      np=1;
    else %q=4 Nilpotent matrices
      sol=((Ap{4}/4+Ap{3})/3+Ap{2})/2+A+eye(n);
      np=0;
    end
  case 8 %m=8 q=2
    c=[4.980119205559973e-03     1.992047682223989e-02     7.665265321119147e-02     8.765009801785554e-01    1.225521150112075e-01     2.974307204847627 0.5 1 1];
    y0s=Ap{2}*(c(1)*Ap{2}+c(2)*A);
    sol=(y0s+c(3)*Ap{2}+c(4)*A)*(y0s+c(5)*Ap{2})+c(6)*y0s+Ap{2}/2+A+eye(n);
    np=2;
  case 15 %m=15 q=2
    c=[4.018761610201036e-04     2.945531440279683e-03    -8.709066576837676e-03     4.017568440673568e-01     3.230762888122312e-02     5.768988513026145e+00 2.338576034271299e-02     2.381070373870987e-01     2.224209172496374e+00    -5.792361707073261e+00    -4.130276365929783e-02     1.040801735231354e+01 -6.331712455883370e+01     3.484665863364574e-01 1 1];
    y0s=Ap{2}*(c(1)*Ap{2}+c(2)*A);
    y1s=(y0s+c(3)*Ap{2}+c(4)*A)*(y0s+c(5)*Ap{2})+c(6)*y0s+c(7)*Ap{2};
    sol=(y1s+c(8)*Ap{2}+c(9)*A)*(y1s+c(10)*y0s+c(11)*A)+c(12)*y1s+c(13)*y0s+c(14)*Ap{2}+A+eye(n);
    np=3;
  case 21 %m=21 q=3
    c=[1.161658834444880e-06     4.500852739573010e-06     5.374708803114821e-05     2.005403977292901e-03     6.974348269544424e-02     9.418613214806352e-01 2.852960512714315e-03    -7.544837153586671e-03     1.829773504500424e+00     3.151382711608315e-02     1.392249143769798e-01    -2.269101241269351e-03 -5.394098846866402e-02     3.112216227982407e-01     9.343851261938047e+00     6.865706355662834e-01     3.233370163085380e+00    -5.726379787260966e+00 -1.413550099309667e-02    -1.638413114712016e-01 1 1];
    y0s=Ap{3}*(c(1)*Ap{3}+c(2)*Ap{2}+c(3)*A);
    y1s=(y0s+c(4)*Ap{3}+c(5)*Ap{2}+c(6)*A)*(y0s+c(7)*Ap{3}+c(8)*Ap{2})+c(9)*y0s+c(10)*Ap{3}+c(11)*Ap{2};
    sol=(y1s+c(12)*Ap{3}+c(13)*Ap{2}+c(14)*A)*(y1s+c(15)*y0s+c(16)*A)+c(17)*y1s+c(18)*y0s+c(19)*Ap{3}+c(20)*Ap{2}+A+eye(n);
    np=3;
  case 24 %m=24 q=4
    c=[1.172460202011541e-08     9.379681616092325e-08     1.406952242413849e-06     2.294895435403922e-05     2.024281516007681e-03     1.430688980356062e-02     1.952545843107103e-01     2.865001388641538e+00    -1.204349003694297e-03     2.547056607231984e-03 2.721930992200371e-02     2.498969092549990e+02     2.018492049443954e-02     1.965098904519709e-01     1.739158441630994e+00 8.290085751394409e+00     2.919349464582001e-04     1.758035313846159e-04     1.606091400855144e-02     3.655234395347475e-02 2.243394407902074e-03    -3.005000525808178e-02     1.969779342112314e-01     1     1];
    y0s=Ap{4}*(c(1)*Ap{4}+c(2)*Ap{3}+c(3)*Ap{2}+c(4)*A);
    y1s=(y0s+c(5)*Ap{4}+c(6)*Ap{3}+c(7)*Ap{2}+c(8)*A)*(y0s+c(9)*Ap{4}+c(10)*Ap{3}+c(11)*Ap{2})+c(12)*y0s+c(13)*Ap{4}+c(14)*Ap{3}+c(15)*Ap{2}+c(16)*A;
    sol=y1s*(y0s+c(17)*Ap{4}+c(18)*Ap{3}+c(19)*Ap{2}+c(20)*A)+c(21)*Ap{4}+c(22)*Ap{3}+c(23)*Ap{2}+A+eye(n);
    np=3;
  case 30 %m=30 q=5
    c=[1.556371639324141e-11     1.556371639324141e-10     2.957106114715868e-09     6.204734935438909e-08     1.313681421698863e-06     3.501669195497238e-05 1.283057135586989e-03     2.479095151834799e-02     4.155284057336423e-01     5.951585263506065e+00     3.753710741641900e-05     2.100333647757715e-04 2.630043177655382e-03     3.306559506631931e-02     6.175954247606858e+01     2.742336655922557e-03     3.005135891320298e-02     2.857950268422422e-01 2.991654767354374e+00     1.110689398085882e+01     8.572383602707347e-06     9.027588625491207e-05     1.121744731945438e-03     8.139086096860678e-03 -2.638236222337760e-04     6.263526066651383e-05     4.985549176118462e-03     7.705596948494946e-02     5.029302610017967e-01 1 1];
    y0s=Ap{5}*(c(1)*Ap{5}+c(2)*Ap{4}+c(3)*Ap{3}+c(4)*Ap{2}+c(5)*A);
    y1s=(y0s+c(6)*Ap{5}+c(7)*Ap{4}+c(8)*Ap{3}+c(9)*Ap{2}+c(10)*A)*(y0s+c(11)*Ap{5}+c(12)*Ap{4}+c(13)*Ap{3}+c(14)*Ap{2})+c(15)*y0s+c(16)*Ap{5}+c(17)*Ap{4}+c(18)*Ap{3}+c(19)*Ap{2}+c(20)*A;
    sol=y1s*(y0s+c(21)*Ap{5}+c(22)*Ap{4}+c(23)*Ap{3}+c(24)*Ap{2}+c(25)*A)+c(26)*Ap{5}+c(27)*Ap{4}+c(28)*Ap{3}+c(29)*Ap{2}+A+eye(n);
    np=3;
end
%endfunction
