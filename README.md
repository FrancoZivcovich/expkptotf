## On-the-fly Krylov projection of the backward error for arbitrary precision Taylor approximation of the matrix exponential
Franco Zivcovich, Jorge Sastre, Javier Ibanez, Emilio Defez


**Abstract**

*New and old challenges brought by the numerical applications and by the always more diffused calculus in arbitrary precision arithmetic 
fueled a lively research in the field of matrix functions. The most important of which is without any doubt the matrix exponential. 
In this paper the authors developed a routine for computing the matrix exponential in arbitrary precision arithmetic for any given tolerance 
that showed outstanding performances.*

---

**Files:**

[ F, m, s, Cp ] = expkptotf( A, prec, evSch, Cp_max )

 Needed inputs:
 
              A:  n x n matrix to exponentiate

 Optional inputs:
 
           prec:  precision up to which exp( A ) is approximated
          evSch:  if false, Paterson-Stockmeyer evaluation scheme is employed
         Cp_max:  maximum matrix products allowed to evaluate T_m(2^-s A)

 Outputs:
 
              F:  approximation of the matrix exponential of A
              m:  degree of approximation used
              s:  log2 of the employed scaling parameter
             Cp:  total matrix by matrix products
			 
---

 Copyright (c), Franco Zivcovich. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
	 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
